﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Parser.Data;

namespace Parser.Controllers
{
    public class LogiController : Controller
    {
        private readonly ParserContext _context;

        public LogiController(ParserContext context)
        {
            _context = context;
        }

        // GET: Logi
        public async Task<IActionResult> Index()
        {
              return View(await _context.Logis.ToListAsync());
        }

        // GET: Logi/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Logis == null)
            {
                return NotFound();
            }

            var logi = await _context.Logis
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (logi == null)
            {
                return NotFound();
            }

            return View(logi);
        }

        // GET: Logi/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Logi/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Time,ShopId,ProductId,Price")] Logi logi)
        {
            if (ModelState.IsValid)
            {
                _context.Add(logi);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(logi);
        }

        // GET: Logi/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Logis == null)
            {
                return NotFound();
            }

            var logi = await _context.Logis.FindAsync(id);
            if (logi == null)
            {
                return NotFound();
            }
            return View(logi);
        }

        // POST: Logi/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Time,ShopId,ProductId,Price")] Logi logi)
        {
            if (id != logi.ProductId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(logi);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LogiExists(logi.ProductId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(logi);
        }

        // GET: Logi/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Logis == null)
            {
                return NotFound();
            }

            var logi = await _context.Logis
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (logi == null)
            {
                return NotFound();
            }

            return View(logi);
        }

        // POST: Logi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Logis == null)
            {
                return Problem("Entity set 'ParserContext.Logis'  is null.");
            }
            var logi = await _context.Logis.FindAsync(id);
            if (logi != null)
            {
                _context.Logis.Remove(logi);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LogiExists(int id)
        {
          return _context.Logis.Any(e => e.ProductId == id);
        }
    }
}
