﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Parser.Data;

namespace Parser.Controllers
{
    public class ProductShopController : Controller
    {
        private readonly ParserContext _context;

        public ProductShopController(ParserContext context)
        {
            _context = context;
        }


        // GET: ProductShop
        public async Task<IActionResult> Index()
        {
            var parserContext = _context.ProductShops.Include(p => p.Product).Include(p => p.Shop);
            return View(await parserContext.ToListAsync());
        }
        public async Task<IActionResult> Menshee()
        {
            var parserContext = _context.ProductShops.Include(p => p.Product).Include(p => p.Shop);
            return View(await parserContext.ToListAsync());
        }

        // GET: ProductShop/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.ProductShops == null)
            {
                return NotFound();
            }

            var productShop = await _context.ProductShops
                .Include(p => p.Product)
                .Include(p => p.Shop)
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (productShop == null)
            {
                return NotFound();
            }

            return View(productShop);
        }


        // GET: ProductShop/Create
        public IActionResult Create()
        {
            ViewData["ProductId"] = new SelectList(_context.Products, "ProductId", "Name");
            ViewData["ShopId"] = new SelectList(_context.Shops, "ShopId", "Name");
            return View();
        }

        // POST: ProductShop/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProductId,ShopId,Link,Price")] ProductShop productShop)
        {
            if (ModelState.IsValid)
            {
                //определение цены

                using var httpClient = new HttpClient() { Timeout = TimeSpan.FromSeconds(50) };

                var response = await httpClient.GetAsync(productShop.Link);
                var responseData = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var startBlock = "";
                    if (productShop.ShopId == 3 | productShop.ShopId == 5)
                    {
                        //водопарад
                        if (productShop.ShopId == 3)
                            startBlock = "<input type=\"hidden\" id=\"PRICE\" name=\"PRICE\" value=\"";
                        //смеситель
                        else
                            startBlock = "itemprop=\"price\" content=\"";

                        var startPos = responseData.IndexOf(startBlock);
                        var priceAllText = responseData.Substring(startPos + startBlock.Length);
                        var priceText = priceAllText.Substring(0, priceAllText.IndexOf('"'));
                        productShop.Price = Convert.ToInt32(priceText);
                    }
                }

                _context.Add(productShop);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            
            ViewData["ProductId"] = new SelectList(_context.Products, "ProductId", "Name", productShop.ProductId);
            ViewData["ShopId"] = new SelectList(_context.Shops, "ShopId", "Name", productShop.ShopId);
            return View(productShop);
        }

        // GET: ProductShop/Edit/5
        public async Task<IActionResult> Edit(int? ShopId, int? ProductId)
        {
            if (ShopId == null || ProductId == null || _context.ProductShops == null)

            {
                return NotFound();
            }

            var productShop = await _context.ProductShops.FirstOrDefaultAsync(x => x.ShopId == ShopId && x.ProductId == ProductId);
            if (productShop == null)
            {
                return NotFound();
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "ProductId", "ProductId", productShop.ProductId);
            ViewData["ShopId"] = new SelectList(_context.Shops, "ShopId", "ShopId", productShop.ShopId);
            return View(productShop);
        }

        // POST: ProductShop/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ProductId,ShopId,Link,Price")] ProductShop productShop)
        {
            if (id != productShop.ProductId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(productShop);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductShopExists(productShop.ProductId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "ProductId", "ProductId", productShop.ProductId);
            ViewData["ShopId"] = new SelectList(_context.Shops, "ShopId", "ShopId", productShop.ShopId);
            return View(productShop);
        }

        // GET: ProductShop/Delete/5
        public async Task<IActionResult> Delete(int? ShopId, int? ProductId)
        {
            if (ShopId == null || ProductId == null || _context.ProductShops == null)
            {
                return NotFound();
            }

            var productShop = await _context.ProductShops
                .Include(p => p.Product)
                .Include(p => p.Shop)
                .FirstOrDefaultAsync(x => x.ShopId == ShopId && x.ProductId == ProductId); 
            if (productShop == null)
            {
                return NotFound();
            }

            return View(productShop);
        }

        // POST: ProductShop/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.ProductShops == null)
            {
                return Problem("Entity set 'ParserContext.ProductShops'  is null.");
            }
            var productShop = await _context.ProductShops.FindAsync(id);
            if (productShop != null)
            {
                _context.ProductShops.Remove(productShop);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductShopExists(int id)
        {
          return _context.ProductShops.Any(e => e.ProductId == id);
        }
    }
}
