﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Parser.Data
{
    public class Logi
    {
        [Column(TypeName = "datetime")]
        public DateTime Time { get; set; }
        public int ShopId { get; set; }
        public int ProductId { get; set; }
        public double Price { get; set; }
    }
}
