﻿using Microsoft.EntityFrameworkCore;

namespace Parser.Data
{
    public class ParserContext : DbContext
    {
        public DbSet<Logi> Logis { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductShop> ProductShops { get; set; }
        public DbSet<Shop> Shops { get; set; }
        public ParserContext(DbContextOptions<ParserContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBulder)
        {
            modelBulder.Entity<Logi>().HasKey(x => new { x.ProductId, x.ShopId, x.Time });
            modelBulder.Entity<ProductShop>().HasKey(x => new { x.ProductId, x.ShopId });
        }


    }
}
