﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Parser.Migrations
{
    public partial class forignKeyMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_ProductShops_ShopId",
                table: "ProductShops",
                column: "ShopId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductShops_Products_ProductId",
                table: "ProductShops",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "ProductId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductShops_Shops_ShopId",
                table: "ProductShops",
                column: "ShopId",
                principalTable: "Shops",
                principalColumn: "ShopId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductShops_Products_ProductId",
                table: "ProductShops");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductShops_Shops_ShopId",
                table: "ProductShops");

            migrationBuilder.DropIndex(
                name: "IX_ProductShops_ShopId",
                table: "ProductShops");
        }
    }
}
